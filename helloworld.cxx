#include <iostream>
#include <fstream>

int main (int argc, char** argv) {
  std::ofstream myfile;
  std::cout<<" hello world. We will write a message to"<<argv[1]<<std::endl;
  myfile.open(argv[1]);
  myfile<<"Writing to myfile. My message is"<<argv[2]<<std::endl;
  myfile.close();
  return 0;
}
